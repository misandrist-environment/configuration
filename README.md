# Misandrist Configuration

This is essentially some default configuration I like when I start up
a new host. It uses increased security ciphers for OpenSSH, installs
files with decent permissions, and sets a git template.
