#!/usr/bin/env zsh

set -e

# source/target pairs
readonly -A config_files=(
    git-config ~/.gitconfig
    ssh-config ~/.ssh/config
    authorized_keys ~/.ssh/authorized_keys
)

readonly -A config_dirs=(
    git-template ~/.config/git-template
)

readonly -A config_modes=(
    ~/.gitconfig 0644
    ~/.ssh/ 0700
    ~/.ssh/config 0600
    ~/.ssh/authorized_keys 0600
)

for cfd in "${(k)config_modes[@]}"; do
    if ! echo "${cfd}" | grep -q "/$"; then
        continue;
    fi

    install -m "${config_modes[${cfd}]}" -d "${cfd}"
done

for cfs in "${(k)config_files[@]}"; do
    cft="${config_files[$cfs]}"
    cfm="${config_modes[$cft]}"

    install -m "${cfm}" "${cfs}" "${cft}"
done

for cfs in "${(k)config_dirs[@]}"; do
    cft="${config_dirs[$cfs]}"

    install -m 0700 -d "${cft}"

    (cd "${cfs}"; rsync -rlpt --progress . ${cft})
done
